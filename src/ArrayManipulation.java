
public class ArrayManipulation {
    /**
     * @param args
     */
    public static void main(String[] args) {
        int[] numbers = {5, 8, 3, 2, 7};
        String[] name = {"Alice", "Bob", "Charlie", "David"};
        double[] values = new double[4];
        int sum = 0;
        double max = 0;
        int min = 100;
        //3
        for(int i = 0; i < numbers.length; i++){
            System.out.print(numbers[i]+" ");
        }
        System.out.println();
        //4
        for(int i = 0; i < name.length; i++){
            System.out.print(name[i]+" ");
        }
        System.out.println();
        //5
        values[0] = 1.0;
        values[1] = 2.5;
        values[2] = 3.7;
        values[3] = 4.6;
        //6
        for(int i = 0; i < numbers.length; i++){
            sum=sum+numbers[i];
        }
        System.out.println(sum);
        //7
        for(int i = 0; i < values.length; i++){
            if(values[i]>max){
                max = values[i];
            }
        }
        System.out.println(max);
        //8
        String[] reversedNames = new String[name.length];
        for(int i=name.length-1;i>=0;i--){
            reversedNames[i] = name[i];
            System.out.print(reversedNames[i]+" ");
        }
        System.out.println();
        for(int i = 0; i < numbers.length; i++){
            for(int j=i+1;j<numbers.length;j++){
                if(numbers[i]>numbers[j]){
                    min = numbers[i];  
                    numbers[i] = numbers[j];  
                    numbers[j] = min;  
                }
            }
            System.out.print(numbers[i]+" ");
        }
        
    }
}
